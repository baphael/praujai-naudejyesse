#!/bin/bash
ROOTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#Install admin CLI
which tennisadm >/dev/null 2>&1
if [ $? -ne 0 ]
then
	cd $ROOTPATH
	sudo npm install -g
fi

read -n 1 -p "Install dependencies [Y/n] ? " dep
echo -e "\n"
if [[ ! ${dep,,} =~ ^n$ ]]
then
	#Backend dependencies installation
	cd $ROOTPATH/backend
	#echo -e "Installing backend dependencies...\n"
	npm install #>/dev/null 2>&1

	#Frontend dependencies installation
	cd $ROOTPATH/frontend
	#echo -e "Installing frontend dependencies...\n"
	npm install #>/dev/null 2>&1
fi

#Database initialisation
read -n 1 -p "Initialize database [Y/n] ? " init
echo -e "\n"
if [[ ! ${init,,} =~ ^n$ ]]
then
	which knex >/dev/null 2>&1
	if [ $? -ne 0 ]
	then
		sudo npm install -g knex
	fi
	cd $ROOTPATH/backend/db
	knex migrate:latest
	knex seed:run
fi

#Serve Project
read -n 1 -p "Serve project [Y/n] ? " serve
echo -e "\n"
if [[ ! ${serve,,} =~ ^n$ ]]
then
	cd $ROOTPATH/backend
	node main.js &
	cd $ROOTPATH/frontend
	npm run serve &
fi