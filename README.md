Tennis courts booking web application (Node/Vue)
===
## Project initialization
### `./setup.sh`
- To proceed with the classical recommended project setup process, just press **_[enter]_** 3 times.
- To skip specific parts of the setup process, press **_[n]_** when asked a prompt.
___
### TL;DR
> During the setup process, you'll be asked 3 questions :
> - 1 : **Install dependencies [Y/n] ?**
>   - For a first setup, you'll need to install all the dependencies required by the project (backend and frontend), so just, press **_[enter]_**.
>   - If you want to skip this step and know what you're doing, press **_[n]_**.
> - 2 : **Initialize database [Y/n] ?**
>   - Same here, for a first setup, just, press **_[enter]_** to initialize the database and populate it with minimal required data (courts and default users).
>   - If you want to skip this step and know what you're doing, press **_[n]_**.
> - 3 : **Serve project [Y/n] ?**
>   - If you want to access the application, you'll need to serve the project (backend and frontend), so just press **_[enter]_**.
>   - If you want to skip this step and know what you're doing, press [n].
> - Backend node server should listen on port 8080
> - Frontend vuejs should listen on port 8081
___
## Use it !
At this point, you should be able to see and use the web interface through your favorite navigator at this URL :

### http://localhost:8081

#### Default credentials :
| Login | Password |
| ----- | -------- |
| raphael.berteaud@etu.enseeiht.fr | raphael.berteaud |
| thomas.verzeroli@etu.enseeiht.fr | thomas.verzeroli |
| jean-christophe.buisson@enseeiht.fr | jean-christophe.buisson |

> If for some obscure reasons, you can't access the web interface or notice any strange behavior, feel free to contact us :
> - raph.berteaud@gmail.com
> - t.verze@gmail.com

## Kill it !
Here are shorthand commands to kill processes for backend and frontend :

| Command | Description |
| ------- | ----------- |
| `fuser -kn tcp 8080` | Kill backend |
| `fuser -kn tcp 8081` | Kill frontend |

> **Note :** `8080` and `8081` are the default ports on which respectively **backend** and **frontend** should be listening on. If somehow, they're different for you, replace them as required !

## CLI administration commands
- Usage: `tennisadm [options] [command]`  
- Options:  
  - `-h`, `--help`                   output usage information  

- Commands:

| command | alias | description |
| ------- | ----- | ----------- |
| `adduser <email>` | `add <email>` | Add a new user |
| `deluser <email>` | `del <email>` | Delete an existing user |
| `change_password <email>` | `pwd <email>` | Change an user's password |
| `listusers <email>` | `list <email>` | List all users |
___
## Notes
This is an unfinished project which would have required at least one more week to be completed.

Non-exhaustive list of missing stuff (or that could have been improved) :
- **Global user experience**
  - error message on wrong authentication
  - click on slot to display the booking panel
  - display month/year
  - separate calendar for each court
    - with ability to display calendar for all of them (with different colors)
  - rebuild navbar
    - include calendar-related buttons into navbar
      - difficulties to emit and intercept events between components...
  - use different colors for each users
  - add username in the navbar

- **Functionalities**
  - improve checks before validation of a booking
    - check if court is available for the selected slot
    - check if player and opponent are available for the selected slot
    - improve booking checks in frontend
      - disable submit button until all fields are valid
    - add backend checks for booking
    - add email notifications !
    - add user profile panel
      - with ability to change password (at least)