import Vue from "vue"
import App from "./App.vue"
import vuetify from "./plugins/vuetify"
import router from './router'
import store from './store'
import Axios from 'axios'
//const token = localStorage.getItem('token')

Vue.config.productionTip = false

/** Include Axios into our Vue instance as $axios */
Vue.prototype.$axios = Axios

/**
 * Vue-router navigation guards
 * Protection of auth-required routes
*/
router.beforeEach((to, from, next) => {
  let loggedIn = false
  /** Automatic inclusion of token in every client's requests
   * If found in localStorage
   */
  if(localStorage.getItem('token')){
    Vue.prototype.$axios.defaults.headers.common.token = localStorage.getItem('token')
    loggedIn = true
  } else {
    delete Vue.prototype.$axios.defaults.headers.common.token
  }
  // If route requires authentication
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // then, we check token with backend '/check'
    if(loggedIn) {
      next()
    } else {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    }
  } else {
    next()
  }
})

new Vue({
  el: '#app',
  router,
  store,
  vuetify,
  render: h => h(App)
})
