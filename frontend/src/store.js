/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Axios.defaults.baseURL = '//localhost:8080/'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || '',
        user: JSON.parse(localStorage.getItem('user')) || ''
    },
    mutations: {
        login: function (state, payload) {
            state.user = payload.user
            state.token = payload.token
        },
        logout: function(state) {
            state.user = ''
            state.token = ''
        }
    },
    actions: {
        login: function (context, data){
            Axios.post('/auth', data).then(
                response => {
                    localStorage.setItem('user', JSON.stringify(response.data.user))
                    localStorage.setItem('token', response.data.token)
                    context.commit('login', response.data)
                }
            ).catch(
                e => {
                    context.dispatch('logout')
                    throw e
                }
            )
        },
        logout: function(context) {
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            context.commit('logout')
        }
    },
    getters: {
        isAuth: state => !!state.token,
    }
})