#!/usr/bin/env node
const program = require('commander')
const inquirer = require('inquirer')
const bcrypt = require('bcrypt')
const saltRounds = 10
const path = require('path')
const knex = require('knex')
const db = knex({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, '../db/db.sqlite3')
    },
    useNullAsDefault: true
})

// add user
program
    .command('adduser <email>')
    .alias('add')
    .description('Add a new user')
    .action(async function(email) {
        let questionAnswer = await inquirer.prompt([
            {
                name: 'password',
                type: 'password',
                message: "Enter password:",
            },
        ])
        let hash = bcrypt.hashSync(questionAnswer.password, saltRounds)
        try {
            let insert = await db('users').insert({email: email, password: hash})
            console.log(`User ${email} (${insert}) created !`)
            process.exit(0)
        } catch (e) {
            console.log(`Fail to create user ${email}:`, e)
            process.exit(1)
        }
    })

// delete existing user
program
    .command('deluser <email>')
    .alias('del')
    .description('Delete an existing user')
    .action(async function(email) {
        try {
            let del = await db('users').where({email: email}).del()
            if(del==0) throw `User not found !`
            console.log(`User ${email} (${del}) deleted !`)
            process.exit(0)
        } catch (e) {
            console.log(`Failed to delete user ${email}:`, e)
            process.exit(1)
        }
    })

// change password
program
    .command('change_password <email>')
    .alias('pwd')
    .description("Change a user's password")
    .action(async function(email) {
        let questionAnswer = await inquirer.prompt([
            {
                name: 'password',
                type: 'password',
                message: "Enter password:",
            },
        ])
        let hash = bcrypt.hashSync(questionAnswer.password, saltRounds)
        try {
            let upd = await db('users').where({email: email}).update({password: hash})
            console.log(`${email}'s password changed !`)
            process.exit(0)
        } catch (e) {
            console.log(`Failed to change ${email}'s password:`, e)
            process.exit(1)
        }
    })

// list users
program
    .command('listusers')
    .alias('list')
    .description('List all users')
    .action(async function() {
        try {
            let list = await db.from('users').select('id', 'email')
            list.forEach((v) => {
                console.log(v.id + ': ' + v.email)
            })
            process.exit(0)
        } catch (e) {
            console.log(`Failed to list users:`, e)
            process.exit(1)
        }
    })

program
    .command('*')
    .action(function () {
        program.help()
    })

program.parse(process.argv)