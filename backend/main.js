#!/usr/bin/node
/**
 * Import dependencies
 */
const express = require('@feathersjs/express')
const feathers = require('@feathersjs/feathers')
const path = require('path')
const services = require('feathers-knex')
const knex = require('knex')
const db = knex({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, '/db/db.sqlite3')
    },
    useNullAsDefault: true
})
const app = express(feathers())
const jwt = require('jsonwebtoken')
const jwtOptions = require('./jwtOptions')
const bcrypt = require('bcrypt')
const bodyParser = require('body-parser')
const listenPort = Number(process.argv[2])||8080
const cors = require('cors')

/** Allow any Cross-Origin Request
 * Note : unsafe...
 */
app.use(cors())

app.use(bodyParser.urlencoded({extended: true}))

app.use(bodyParser.json())

app.configure(express.rest())

function checkAuth() {
    if(token = request.headers.token) {
        jwt.verify(token, jwtOptions.secret, (err, decoded) => {
            if(err) {
                return response.status(401).json({
                    status: false,
                    data: `Authentication required !`,
                    more: err
                })
            } else {
                request.decodedJwt = decoded
                next()
            }
        })
    } else {
        return response.status(401).json({
            status: false,
            data: 'Authentication required !',
            more: `Token not found or expired.`
        })
    }
}

app.post('/auth', async (request,response) => {
    console.log('Call of Authentication:', request.body)
    //let hash = bcrypt.hashSync(request.body.password, saltRounds)
    try {
        let login = request.body.email
        /** Fetch user by email */
        let foundUser = await db.select('*').from('users').where({email: login})
        if(!foundUser || foundUser.length==0) throw `User ${login} not found !`
        /** Compare password & hash */
        if(!bcrypt.compareSync(request.body.password, foundUser[0].password))
            throw `Password mismatch for user ${login}`
    
        /** Remove hash from user data to return */
        let {password, ...safeUser} = foundUser[0]
        let token = jwt.sign(safeUser, jwtOptions.secret)

        response.status(200).json({user: safeUser, token})
    } catch(e) {
        console.log("Authentication failure:", e)
        response.status(401).json({
            status: false,
            data: `Authentication failure`,
            more: 'Bad credentials !'
        })
    }
})

app.use('/api/users', services({
    Model: db,
    name: 'users'
}))

app.use('/api/courts', services({
    Model: db,
    name: 'courts'
}))

app.use('/api/reservations', services({
    Model: db,
    name: 'reservations'
}))

/*
app.get('*', (request, response) => {
    response.status(200).send(`Status: OK`)
})
*/
app.listen(listenPort, () => console.log("Listening on port : ", listenPort))