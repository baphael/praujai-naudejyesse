
exports.up = knex => {
    return knex.schema.createTable('users', table => {
            table.increments('id').unsigned().primary()
            table.string('email').notNull().unique()
            table.string('password')
        }).createTable('roles', table => {
            table.increments('id').unsigned().primary()
            table.string('label').unique().notNull()
            table.string('description')
        }).createTable('users_roles', table => {
            table.integer('user')
            table.integer('role')
            table.foreign('user').references('id').inTable('users')
            table.foreign('role').references('id').inTable('roles')
        }).createTable('courts', table => {
            table.increments('id').unsigned().primary()
            table.string('label').unique()
            table.boolean('available').notNull().default(1)
        }).createTable('reservations', table => {
            table.increments('id').unsigned().primary()
            table.integer('player').unsigned()
            table.integer('opponent').unsigned()
            table.integer('court').unsigned()
            table.datetime('slot')
            table.foreign('player').references('id').inTable('users')
            table.foreign('opponent').references('id').inTable('users')
            table.foreign('court').references('id').inTable('courts')
        })
}

exports.down = knex => {
    return knex.schema.dropTableIfExists([
        'users',
        'roles',
        'users_roles',
        'reservations',
        'courts'
    ])
};