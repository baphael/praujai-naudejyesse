
exports.seed = function(knex) {
  return knex('courts').del().then(() => {
    return knex('courts').insert([
      {
        id: 1,
        label: 'Court 1'},
      {
        id: 2,  
        label: 'Court 2'
      },
      {
        id: 3,  
        label: 'Court 3'
      },
      {
        id: 4,
        label: 'Court 4'
      },
      {
        id: 5,  
        label: 'Court 5'
      },
      {
        id: 6,
        label: 'Court 6'
      }
    ])
  })
}
