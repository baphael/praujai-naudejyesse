
exports.seed = knex => {
  return knex('users').del()
    .then(() => {
      return knex('users').insert([{"id":1,"email":"raphael.berteaud@etu.enseeiht.fr","password":"$2b$10$N/0.ZguNV3zpsJjNJjCLX.onthu1zusZmL68SNP8Dw5Slq.u98qJy"},{"id":2,"email":"thomas.verzeroli@etu.enseeiht.fr","password":"$2b$10$C3ZWOPCoOBUJoJnSXVpCxegv0uZomQfcrGnmMDt76Tk2/WAIhXpum"},{"id":3,"email":"jean-christophe.buisson@enseeiht.fr","password":"$2b$10$tmm9hfLo/sKbdYCYrbVKKegfW7NHPomkoFnLttZz1xhal4UtiFJtG"}])
    })
}
